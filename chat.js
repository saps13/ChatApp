var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var User = require("./users");
var ChatHistory = require("./history.js");

global.chathistory = "";
global.displayname = "";
app.get('/', function(req, res) {
    res.sendFile(__dirname + '/reglog.html');
});
app.get('/chat.html', function(req, res) {
    res.sendFile(__dirname + '/chat.html');
});

io.on('connection', function(socket) {
    	console.log('A new WebSocket connection has been established');
	io.emit('chat history', chathistory);
	socket.on('user comment', function(name,data) {
		chathistory += data;
		var chat_history = new ChatHistory({
			username: name,
			comment: data
		});
		chat_history.save(function(err) {
                        if(err){
                                console.log(err);
                                io.emit('register error', 'Something went wrong!!');
                        }
                });
		socket.broadcast.emit('new comment', data);
	});
	socket.on('register', function(name, username, password){
		var usr = new User({
  		name: name,
  		username: username,
  		password: password
		});
		usr.save(function(err) {
			if(err.name == "MongoError" && err.code == 11000){
                                io.emit('register error', 'Username already exists');
                        }
			else if(err){
				console.log(err);
				io.emit('register error', 'Something went wrong!!');
			}
			else{
  				console.log('User saved successfully!');
				displayname = usr.username;
				socket.emit('redirect','/chat.html');
			}
		});
	});
	socket.on('login', function(uname, pwd){
		User.findOne({username : uname, password : pwd},function(err,user){
			if (err){
				console.log(err);
				io.emit('login error', 'Something went wrong');
			}
			else if(user == null){
				io.emit('login error', 'Wrong username and/or password');
			}
			else{
				displayname = user.username;
				console.log('User logged in ..');
				socket.emit('redirect','/chat.html');
			}
		});
	});
	io.emit('user name', displayname);
});

http.listen(8000, function() {
    console.log('Listening on *:8000');
});


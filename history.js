var mongoose = require('mongoose');

// make a connection 
mongoose.connect('mongodb://localhost:27017/chat_history', { useNewUrlParser: true });

// get reference to database
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
        console.log("Connection Successful!");
});
// define Schema
    var ChatSchema = mongoose.Schema({
      username: String,
        comment: String
    });

    // compile schema to model
    var ChatHistory = mongoose.model('ChatHistory', UserSchema);

    // a document instance
    module.exports = ChatHistory;

